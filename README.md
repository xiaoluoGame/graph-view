# Unity GraphView 通用节点编辑框架

## 简介

本项目旨在为Unity开发一个通用的节点编辑框架，使用GraphView实现逻辑、数据和编辑器的分离。该框架允许用户创建独立的逻辑节点，这些节点可以在编辑器中进行编辑和连接，并且可以独立运行。此外，框架还支持仅编辑逻辑或数据及其连接关系，目前用户需要自行编写解释器来解析和运行这些逻辑。


[详细文档说明](https://xiaoluo.xyz/docs/GraphView)



## 特性

- **逻辑与数据分离**：逻辑和数据在编辑器中是相互独立的，可以单独编辑和运行。
- **可编辑逻辑与数据**：用户可以仅编辑逻辑或数据及其连接关系。
- **自定义解释器**：用户需要编写自己的解释器来解析和运行编辑好的逻辑。
- **独立运行**：编辑好的逻辑可以独立运行，无需依赖编辑器。

## 安装

1. 克隆或下载本项目到你的Unity项目中。
2. 在Unity编辑器中打开项目。
3. 导入GraphView相关的包（如果尚未导入）。

## 使用方法

1. 打开编辑器窗口，创建一个新的GraphView。
2. 添加逻辑节点和数据节点，并进行连接。
3. 编辑节点的逻辑和数据。
4. 保存编辑好的逻辑和数据。
5. 编写自定义解释器来解析和运行编辑好的逻辑。

## 示例

由于项目目前尚未完成，暂无详细的使用文档。但你可以参考以下示例代码来了解如何使用本框架：

```csharp
// 示例代码，展示如何创建和编辑节点
public class DebugNode : NodeViewBase
{
    public override string MenuPath => "调试/Debug";
    private Label log;
    protected override void Init()
    {
        base.Init();
        title = "控制台输出";
        
        // 填写推荐连接的端口 不填写默认根据端口类型匹配
        AddRecommendedPortNode<RandomNumNode>();
        AddInputRecommendedPortNode<AddNode>();

        AddNodeLogic<DebugNodeLogic>();

        log = new Label();
        extensionContainer.Add(log);
    }

    protected override void InitializeForFirstCreation()
    {
        base.InitializeForFirstCreation();

        CreateInputExecutionLinePort();
        CreateOutputExecutionLinePort();

        CreateInputPort<object>("输入");
    }

    protected override void OnExecuteEnd(INodeLogic nodeLogic)
    {
        base.OnExecuteEnd(nodeLogic);
        log.text = nodeLogic.GetInputData<object>("输入").ToString();
    }
}
public class DebugNodeLogic : NodeLogicBase
{
    public override void OnExecute(string n)
    {
        base.OnExecute(n);
        Debug.Log(GetInputData<object>("输入"));
    }
}
```

## 贡献

欢迎贡献代码和提出问题。请在GitHub上提交Issue或Pull Request。

## 许可证

本项目采用MIT许可证。详细信息请参阅[LICENSE](LICENSE)文件。

## 联系我们

如有任何问题或建议，请联系项目维护者：

- 邮箱：[2324834989@qq.com](mailto:2324834989@qq.com)

---

**注意**：本项目目前处于开发阶段，尚未完成，因此没有详细的使用文档。我们将尽快完善文档并提供更多示例代码。