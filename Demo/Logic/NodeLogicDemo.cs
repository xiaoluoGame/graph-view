﻿using UnityEngine;

namespace XLFrame.NodeEditor
{
    public class IfNodeLogic : NodeLogicBase
    {
        protected override void OnExecute(string n)
        {
            bool condition = GetInputData<bool>("条件");
            if (condition)
            {
                onNextExecute?.Invoke(GetOutputPortGUID("True"));
            }
            else
            {
                onNextExecute?.Invoke(GetOutputPortGUID("False"));
            }
        }
    }
    public class DebugNodeLogic : NodeLogicBase
    {
        protected override void OnExecute(string n)
        {
            Debug.Log(GetInputData<object>("输入"));
        }
    }
    public class StartNodeLogic : NodeLogicBase
    {
        protected override void OnExecute(string n)
        {
            
        }
    }
    public class RandomNumNodeLogic : NodeLogicBase
    {
        protected override void OnExecute(string n)
        {
            int min = GetInputData<int>("最小值");
            int max = GetInputData<int>("最大值");

            int random = UnityEngine.Random.Range(min, max);
            SetOutputData("随机数", random);
        }
    }
    
    public class AddNodeLogic : NodeLogicBase
    {
        /// <summary>
        /// 当前节点没有执行逻辑线
        /// </summary>
        /// <param name="n"></param>
        protected override void OnExecute(string n)
        {
            
        }

        /// <summary>
        /// 当其他节点尝试获取输出值时,返回的object就是其他节点拿到的值
        /// </summary>
        /// <param name="arg1">参数名</param>
        /// <param name="arg2">参数值</param>
        /// <param name="eGuid"></param>
        /// <returns>你要返回的参数值</returns>
        protected override object OnGetOutputData(string arg1, object arg2,string eGuid)
        {
            if (arg1 != "和") return arg2;
            Debug.Log("请求和:" + arg2);
            float num1 = GetInputData<float>("加数1");
            float num2 = GetInputData<float>("加数2");
            float sum = num1 + num2;
            return sum;
        }
    }
}