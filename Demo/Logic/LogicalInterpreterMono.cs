﻿using System;
using UnityEngine;
using XLFrame.NodeEditor.Interpreter;

namespace XLFrame.NodeEditor.Demo
{
    /// <summary>
    /// 逻辑解释器运行入口
    /// </summary>
    public class LogicalInterpreterMono : MonoBehaviour
    {
        public NodeTree data;
        private LogicalInterpreter logicalInterpreter;

        private void Start()
        {
            data.ResetData();
            logicalInterpreter = new LogicalInterpreter(data.NodeTreeData);
        }

        [ContextMenu("执行")]
        private void Run()
        {
            logicalInterpreter.Run();
        }
    }
}