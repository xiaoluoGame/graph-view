using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLFrame.NodeEditor;
using XLFrame.NodeEditor.Demo;
using XLFrame.NodeEditor.window;

namespace XLFrame.NodeEditor.Demo
{
    public class WindowDemo : NodeWindowBase<WindowDemo, NodeTree>
    {
        public WindowDemo()
        {
            titleContent = new GUIContent("节点编辑器演示");
        }
    }
}