using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace XLFrame.NodeEditor.Demo
{
    /// <summary>
    /// 节点操作视图,仅操作视图节点,节点内的逻辑分离到逻辑层这样可在不同的环境下运行
    /// 最少继承代码示例
    /// </summary>
    public class NodeGraphView : NodeGraphViewBase
    {
        public new class UxmlFactory : UxmlFactory<NodeGraphView, GraphView.UxmlTraits> { }
        
        public NodeGraphView() : base()
        {
            // ShowDefaultContextMenu = true;
        }

        protected override void OnDataLoad()
        {
            // // 如果没有数据则创建一个开始节点
            // if (NodeTreeDataBase.NodeDataBaseDic.Count == 0)
            // {
            //     CreateNode<StartNode>(default);
            // }
        }
    }
}
