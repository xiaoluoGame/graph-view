using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace XLFrame.NodeEditor
{
    /// <summary>
    /// 右键菜单提供的数据
    /// </summary>
    public interface IMenuWindowData
    {
        /// <summary>
        /// 右键菜单路径
        /// </summary>
        public string MenuPath { get; }

        /// <summary>
        /// 初始化数据
        /// </summary>
        public void InitData();

        /// <summary>
        /// 输入端口,返回推荐连接的端口
        /// </summary>
        /// <param name="port"></param>
        /// <param name="link"></param>
        /// <returns></returns>
        public Port ConnectRecommendedPort(Port port, bool link=false);

        /// <summary>
        /// 返回当前节点推荐的端口节点
        /// </summary>
        /// <returns></returns>
        public List<Type> GetRecommendedPortNode(bool isInput);

    }
}