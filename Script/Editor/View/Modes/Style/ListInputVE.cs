﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UIElements;

namespace XLFrame.NodeEditor
{
    /// <summary>
    /// 支持列表输入的样式
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ListInputVE<T> : VEStyle,IEnumerable<T> where T : VEStyle,BaseInputVE
    {
        private List<T> m_value { get; set; }
        private Foldout foldout { get; set; }
        
        public ListInputVE(string name = "",IEnumerable<T> collection = default)
        {
            m_value = collection != null ? new List<T>(collection) : new List<T>();
            InitStyle(name);
        }

        private void InitStyle(string name)
        {
            foldout = new Foldout();
            foldout.text = name;
            Button Add = new Button(() =>
            {
                
            });
            
            this.Add(foldout);
        }
        
        private VisualElement CreateInputField()
        {
            VisualElement container = new VisualElement();
            container.style.flexDirection = FlexDirection.Row;
            // T input = new T();
            
            return container;
        }
        
        public List<T> ToList()
        {
            return m_value.ToList();
        }


        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        public IEnumerator<T> GetEnumerator()
        {
            return m_value.GetEnumerator();
        }

        /// <summary>Returns an enumerator that iterates through a collection.</summary>
        /// <returns>An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}