﻿using UnityEngine.UIElements;

namespace XLFrame.NodeEditor
{
    /// <summary>
    /// 基础样式封装,预设VisualElement内容,直接new使用 添加进任意VisualElement即可
    /// 约定 所有子类使用VE结尾
    /// </summary>
    public class VEStyle : VisualElement
    {
    }
}