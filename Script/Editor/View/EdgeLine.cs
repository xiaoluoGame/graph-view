using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace XLFrame.NodeEditor
{
    /// <summary>
    /// 边缘线功能
    /// </summary>
    public class EdgeLine : Edge
    {
        public EdgeLine() : base()
        {
            this.RegisterCallback<DetachFromPanelEvent>(OnDetachFromPanel);
        }

        /// <summary>
        /// 当连线被移除时
        /// </summary>
        /// <param name="evt"></param>
        private void OnDetachFromPanel(DetachFromPanelEvent evt)
        {
            // 重新显示搜索菜单
            if(Event.current is null) return;
            if (Event.current.type == EventType.MouseUp)
            {
                Port port = null;
                if (input is not null)
                {
                    // 从input 拉出 连接到对方 output
                    // Debug.Log(input.name + input.portType.Name + input.node.title);
                    port = input;

                }
                else if (output is not null)
                {
                    // 从output 拉出 连接到对方 input
                    // Debug.Log(output.name + output.portType.Name + output.node.title);
                    port = output;
                }

                var node = port?.node as NodeViewBase;
                node?.NodeGraphView.ShowSearchMenu(port);
            }
        }
    }
}