﻿namespace XLFrame.NodeEditor
{
    /// <summary>
    /// Node节点内仅框架调用的方法,必须强制显示实现
    /// </summary>
    internal interface INodeViewFrame
    {
        /// <summary>
        /// 框架调用,加载数据结束
        /// </summary>
        void OnNodeDataLoadingDataEndEvent();

        /// <summary>
        /// 框架调用 当节点被添加到视图中时调用
        /// </summary>
        void OnNodeAddViewEvent();

        /// <summary>
        /// 创建新节点,包含新数据 !!构建节点时调用必须!!
        /// </summary>
        void CreateNode(NodeGraphViewBase nodeGraphViewBase);
    }
}