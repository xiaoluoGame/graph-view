using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEditor;
using UnityEngine;


namespace XLFrame.NodeEditor
{
    /// <summary>
    /// Unity 节点树数据层
    /// </summary>De
    public class NodeTreeFile : SerializedCollectionDataBase
    {
        //[HideInInspector]
#if UNITY_EDITOR
        [DisplayOnly]
#endif
        [SerializeField, TextArea(3, 1000)]
        private string NodeTreeDataBaseJson;

        [NonSerialized]
        private NodeTreeDataBase nodeTreeDataBase = null;
        public NodeTreeDataBase NodeTreeData
        {
            get
            {
                if (nodeTreeDataBase == null)
                {
                    if (string.IsNullOrEmpty(NodeTreeDataBaseJson))
                    {
                        nodeTreeDataBase = new NodeTreeDataBase();
                    }
                    else
                    {
                        nodeTreeDataBase = Json.DeserializeObject<NodeTreeDataBase>(NodeTreeDataBaseJson);
                    }
                }

                return nodeTreeDataBase;
            }
            set
            {
                nodeTreeDataBase = value;
#if UNITY_EDITOR
                SaveData();
#endif
            }
        }
#if UNITY_EDITOR
        /// <summary>
        /// 保存数据
        /// </summary>
        public void SaveData()
        {
            NodeTreeDataBaseJson = Json.SerializeObject(nodeTreeDataBase,Newtonsoft.Json.Formatting.Indented);
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
#endif

        /// <summary>
        /// 读取Json数据,替换掉内存临时数据
        /// </summary>
        public void ResetData()
        {
            nodeTreeDataBase = Json.DeserializeObject<NodeTreeDataBase>(NodeTreeDataBaseJson);
        }
    }
}
