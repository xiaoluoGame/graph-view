﻿using System.Collections.Generic;
using UnityEngine;

namespace XLFrame.NodeEditor
{
    /// <summary>
    /// Ctrl + C & Ctrl + V 时用到的数据类
    /// </summary>
    public class NodeSerializeData
    {
        public List<NodeDataBase> nodeDataBases;
        
        public NodeSerializeData(List<NodeDataBase> nodeDataBases)
        {
            this.nodeDataBases = nodeDataBases;
        }
    }
}