﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace XLFrame.NodeEditor
{
    /// <summary>
    /// 存储类, 支持可被json序列化的类型
    /// </summary>
    public class AttributesData
    {
        /// <summary>
        /// 其他自定义属性, 存储类, 支持可被json序列化的类型
        /// </summary>
        [JsonProperty]
        private Dictionary<string, JsonObject> m_attributes = new Dictionary<string, JsonObject>();
        
        #region 属性相关

        public bool ContainsKey(string key)
        {
            return m_attributes.ContainsKey(key);
        }
        
        /// <summary>
        /// 通过索引器访问Attributes属性中的对象，并尝试将其转换为指定的泛型类型。
        /// </summary>
        /// <typeparam name="T">请求的对象类型</typeparam>
        /// <param name="key">属性的键</param>
        /// <returns>转换为指定类型的对象，如果转换失败则返回类型的默认值。</returns>
        public T GetAttribute<T>(string key)
        {
            if (m_attributes.TryGetValue(key, out JsonObject value))
            {
                if (value is null)
                    return default;
                if (value.Value is T)
                {
                    return (T)value.Value;
                }
                try
                {
                    // 尝试强制转换或使用Convert.ChangeType来处理不同类型的转换
                    return (T)Convert.ChangeType(value.Value, typeof(T));
                }
                catch (InvalidCastException)
                {
                    // 转换失败，返回类型T的默认值
                    return default;
                }
            }
            return default;
        }

        /// <summary>
        /// 设置属性，如果属性已经存在则覆盖，否则添加。
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetAttribute(string key, object value)
        {
            if (m_attributes.ContainsKey(key))
            {
                if (m_attributes[key] != null)
                {
                    m_attributes[key].Value = value;
                }
                else
                {
                    m_attributes[key] = new JsonObject(value);
                }
            }
            else
            {
                m_attributes.Add(key, value is null ? null : new JsonObject(value));
            }
        }

        /// <summary>
        /// 索引器，允许通过键直接访问Attributes中的值，并根据泛型参数返回指定的泛型。
        /// </summary>
        /// <param name="key">属性的键</param>
        /// <returns>转换为指定类型的对象，如果转换失败则返回类型的默认值。</returns>
        public object this[string key]
        {
            get
            {
                return GetAttribute<object>(key);
            }
            set
            {
                SetAttribute(key, value);
            }
        }
        

        #endregion
    }
}