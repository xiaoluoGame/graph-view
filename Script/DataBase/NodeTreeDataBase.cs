using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace XLFrame.NodeEditor
{
    /// <summary>
    /// 节点树数据层
    /// </summary>
    [System.Serializable]
    public class NodeTreeDataBase
    {
        /// <summary>
        /// 节点基础数据字典
        /// </summary>
        public Dictionary<string, NodeDataBase> NodeDataBaseDic = new Dictionary<string, NodeDataBase>();
        
        /// <summary>
        /// 节点数属性数据
        /// </summary>
        public AttributesData AttributesData = new AttributesData();

        // 用来储存节点视图信息
        [Serializable]
        public class ViewData
        {
            public Vector3 Position;
            public Vector3 Scale = new Vector3(1, 1, 1);
        }

        /// <summary>
        /// 显示位置数据
        /// </summary>
        public ViewData GraphViewData = new ViewData();

        public void AddData(NodeDataBase nodeData)
        {
            NodeDataBaseDic.Add(nodeData.GUID, nodeData);
        }

        public void RemoveData(string guid)
        {
            NodeDataBaseDic.Remove(guid);
        }
    }
}
