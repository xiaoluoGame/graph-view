﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using XLFrame.NodeEditor.Extensions;

namespace XLFrame.NodeEditor
{
    /// <summary>
    /// 样式数据
    /// </summary>
    public class NodeStyleData
    {
        /// <summary>
        /// 样式唯一值
        /// </summary>
        public string StyleTypeID { get => TypeName+"_"+GUID; }
        
        /// <summary>
        /// 类型
        /// </summary>
        [JsonProperty]
        public string TypeName { get; private set; }
        
        /// <summary>
        /// 样式所在容器
        /// </summary>
        [JsonProperty, JsonConverter(typeof(StringEnumConverter))]
        public NodeStyleContainer StyleContainer { get; private set; }
        
        [JsonProperty]
        public string GUID { get; private set; }
        
        /// <summary>
        /// 其他自定义数据
        /// </summary>
        [JsonProperty]
        public AttributesData Attributes { get; private set; } = new AttributesData();
        
        

        /// <summary>
        /// 创建数据
        /// </summary>
        /// <returns></returns>
        public static NodeStyleData CreateNodeStyleData(Type type,NodeStyleContainer container = NodeStyleContainer.extensionContainer)
        {
            return new NodeStyleData()
            {
                GUID = Guid.NewGuid().ToString(),
                TypeName = type.GetTypeNameAndAssembly(),
                StyleContainer = container
            };
        }
        
        /// <summary>
        /// 节点样式存放的容器
        /// </summary>
        public enum NodeStyleContainer
        {
            extensionContainer,
            mainContainer,
            titleContainer,
            inputContainer,
            outputContainer,
            titleButtonContainer,
            topContainer
        }
    }
}