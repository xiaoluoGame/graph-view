using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XLFrame.NodeEditor
{
    /// <summary>
    /// 通过端口颜色来区分端口类型,使用type获取颜色
    /// </summary>
    public class PortColor
    {
        public static PortColor Instance
        {
            get
            {
                if (m_instantiate == null)
                    m_instantiate = new PortColor();

                return m_instantiate;
            }
        }

        private static PortColor m_instantiate;

        Dictionary<Type, Color> portColorDic = new Dictionary<Type, Color>();
        public PortColor()
        {
            AddPortColor<int>("#00b6ff");
            AddPortColor<long>("#008cff");
            AddPortColor<float>("#00f2ff");
            AddPortColor<float[,]>("#0099B7");
            AddPortColor<double>("#00ffdd");
            AddPortColor<string>("#f300ff");
            AddPortColor<bool>("#0016ff");

            AddPortColor<Vector2>("#AFFFB7");
            AddPortColor<Vector3>("#60DE6E");
            AddPortColor<Vector4>("#2CC63E");
            AddPortColor<Vector2Int>("#90FFB9");
            AddPortColor<Vector3Int>("#32FF7F");
            AddPortColor<Color>("#82FF1A");
            AddPortColor<ExecutionLine>("#f3f3f3");
            AddPortColor<object>("#944FFF");
        }

        /// <summary>
        /// 16进制颜色转Color
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static Color HexToColor(string hex)
        {
            Color color = default;
            if (ColorUtility.TryParseHtmlString(hex, out color))
            {
                return color;
            }
            else
            {
                Debug.LogError("Invalid hex color string");
                return Color.black; // 返回黑色作为默认值或错误值
            }
        }

        /// <summary>
        /// 添加颜色配置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="color"></param>
        public void AddPortColor<T>(string color)
        {
            AddPortColor(typeof(T), HexToColor(color));
        }
        public void AddPortColor<T>(Color color)
        {
            AddPortColor(typeof(T), color);
        }

        public void AddPortColor(Type type, Color color)
        {
            if (!portColorDic.ContainsKey(type))
            {
                portColorDic.Add(type, color);
            }
        }

        public Color GetColor(Type type)
        {
            if (portColorDic.ContainsKey(type))
            {
                return portColorDic[type];
            }
            return Color.black;
        }
    }
}