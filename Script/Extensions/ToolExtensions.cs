using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using XLFrame.NodeEditor;

#if UNITY_EDITOR
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;
#endif

namespace XLFrame.NodeEditor.Extensions
{
    /// <summary>
    /// 扩展方法
    /// </summary>
    public static partial class ToolExtensions
    {
#if UNITY_EDITOR
        
        /// <summary>
        /// 设置端口运行数据可保存
        /// </summary>
        /// <param name="port"></param>
        /// <param name="isSave"></param>
        /// <returns></returns>
        public static Port SetPortDataIsSave(this Port port, bool isSave)
        {
            port.GetPortData().IsSave = isSave;
            return port;
        }
        
        /// <summary>
        /// 获取端口数据
        /// </summary>
        /// <param name="portData"></param>
        /// <returns></returns>
        public static PortData GetPortData(this Port portData)
        {
            return portData.userData as PortData;
        }

        public static IStyle SetMarginWidth(this IStyle style, float left, float right, float top, float bottom)
        {
            style.marginLeft = left;
            style.marginRight = right;
            style.marginTop = top;
            style.marginBottom = bottom;
            return style;
        }
        
        public static IStyle SetPaddingWidth(this IStyle style, float left, float right, float top, float bottom)
        {
            style.paddingLeft = left;
            style.paddingRight = right;
            style.paddingTop = top;
            style.paddingBottom = bottom;
            return style;
        }
        
        /// <summary>
        /// 画框宽度
        /// </summary>
        /// <param name="style"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public static IStyle SetBorderWidth(this IStyle style, float width)
        {
            style.borderBottomWidth = width;
            style.borderLeftWidth = width;
            style.borderRightWidth = width;
            style.borderTopWidth = width;
            return style;
        }
        
        /// <summary>
        /// 画框颜色
        /// </summary>
        /// <param name="style"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public static IStyle SetBorderColor(this IStyle style, Color color)
        {
            style.borderBottomColor = color;
            style.borderLeftColor = color;
            style.borderRightColor = color;
            style.borderTopColor = color;
            return style;
        }
#endif

        public static T Conversion<T>(this object obj)
        {
            if(obj is null)
                return default;

            try
            {
                if (obj is IConvertible)
                {
                    return (T)Convert.ChangeType(obj, typeof(T));
                }

                return (T)obj;
            }
            catch (InvalidCastException)
            {
                Debug.LogError($"无法将'{obj.GetType().Name}'的值转换为类型'{typeof(T).Name}'。");
                return default;
            }
        }

        /// <summary>
        /// 获取可被反射加载的类型的完整名称，包括命名空间和程序集名称
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static string GetTypeNameAndAssembly(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type), "Type cannot be null.");
            }

            string typeName = type.FullName;                    // 获取类型的完整名称，包括命名空间
            string assemblyName = type.Assembly.GetName().Name; // 获取定义该类型的程序集的名称

            return $"{typeName}, {assemblyName}";
        }

        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            foreach (T item in items)
            {
                collection.Add(item);
            }
        }

        /// <summary>
        /// 去掉名字中()内容
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string RemoveNameType(string name)
        {
            string pattern = @"\([^\)]*\)"; // 匹配括号及其内部的内容
            string result = Regex.Replace(name, pattern, "");
            return result;
        }
    }
}