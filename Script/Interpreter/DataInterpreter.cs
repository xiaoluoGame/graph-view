using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace XLFrame.NodeEditor.Interpreter
{
    /// <summary>
    /// 节点连接关系查询器
    /// </summary>
    public class DataInterpreter
    {
        private NodeTreeDataBase m_data;
        public DataInterpreter(NodeTreeDataBase data)
        {
            this.m_data = data;
            init();
        }

        private void init()
        {
            
        }
    }

    /// <summary>
    /// 虚拟节点数据
    /// </summary>
    public class DataVirtualNode
    {
        private NodeDataBase m_dataBase;

        public DataVirtualNode(NodeDataBase dataBase)
        {
            this.m_dataBase = dataBase;
            
        }
      
    }
}