using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace XLFrame.NodeEditor
{
    /// <summary>
    /// 逻辑体基类接口
    /// </summary>
    public interface INodeLogic
    {
        /// <summary>
        /// 当逻辑执行结束时调用
        /// </summary>
        public event UnityAction<INodeLogic> OnExecuteEnd;
        
        /// <summary>
        /// 当前执行的任务索引
        /// </summary>
        public string EGuid { get; set; }
        
        /// <summary>
        /// 获取输入数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T GetInputData<T>(string name);

        /// <summary>
        /// 获取返回数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T GetOutputData<T>(string name);

        /// <summary>
        /// 执行线,执行逻辑,参数是输入端执行的逻辑线节点名称
        /// </summary>
        public void Execute(string lineName);
        
        /// <summary>
        /// 刷新输入输出端口数据
        /// </summary>
        public void RefreshData();

        /// <summary>
        /// 执行前回调用,同步输入数据,输入数据为连接的输出数据
        /// </summary>
        public void Init(NodeDataBase nodeData,Func<List<PortData>> input, Func<List<PortData>> output, UnityAction<string> onExecute);
    }
}
