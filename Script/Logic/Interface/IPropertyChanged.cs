﻿using UnityEngine.Events;

namespace XLFrame.NodeEditor
{
    /// <summary>
    /// 当属性发生变化时
    /// </summary>
    public interface IPropertyChanged
    {
        /// <summary>
        /// 当值发生变化时回调[T1 发生变化的对象 T2 变化的值,string 属性名称]
        /// </summary>
        public event UnityAction<PortDataEvent> OnPropertyChangeEvent;
    }
}